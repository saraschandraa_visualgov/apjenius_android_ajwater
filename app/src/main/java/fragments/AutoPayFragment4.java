package fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.TextView;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by macuser on 1/27/16.
 */
@EFragment(R.layout.fragment_autopay4)
public class AutoPayFragment4 extends Fragment {

    @ViewById(R.id.tv_successfully)
    TextView tvSuccessfully;

    @AfterViews
    void onViewLoaded() {

    }

    @Click(R.id.iv_autopay_done)
    void onAutoPayClicked() {
        AccountsHomeActivity.getInstance().homeButtonPressed();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(getString(R.string.screen_autopay));
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
