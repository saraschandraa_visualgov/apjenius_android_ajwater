package fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.EditText;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by macuser on 1/27/16.
 */

@EFragment(R.layout.fragment_makepayment3)
public class MakePaymentsFragments3 extends Fragment {

    @ViewById(R.id.et_cardnum)
    EditText etCardNumber;

    @ViewById(R.id.et_cardmonth)
    EditText etCardMonth;

    @ViewById(R.id.et_cardyear)
    EditText etCardYear;

    @ViewById(R.id.et_cvvnum)
    EditText etIndentificationNumber;

    @AfterViews
    void onViewLoaded() {

    }

    @Click(R.id.iv_step4)
    void onStep4Clicked() {
        MakePaymentFragment.getInstance().changeMakePaymentFragments(MakePaymentFragment.MKPAY_FRAGMENTS.STEP_FOUR, null);
    }

    @Click(R.id.iv_close)
    void onCloseClicked() {
        AccountsHomeActivity.getInstance().onBackPressed();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
