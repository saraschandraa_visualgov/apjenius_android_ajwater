package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import adapters.BillingDetailsAdapter;
import model.AR;
import model.AccountDetails;
import model.Accounts;

/**
 * Created by SaraschandraaM on 19/01/16.
 */

@EFragment(R.layout.fragment_billingdetails)
public class BillingDetailsFragment extends Fragment {

    @ViewById(R.id.rc_billingdetailslist)
    RecyclerView rcBillingDetails;

    Bundle arguments;
    AccountDetails accountDetails;
    ArrayList<AR> arList;

    BillingDetailsAdapter billingDetailsAdapter;

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        if (arguments != null) {
            accountDetails = (AccountDetails) arguments.getSerializable("AccountDetail");
            arList = new ArrayList<>();
            arList = accountDetails.getArList();
        }

        rcBillingDetails.setLayoutManager(new LinearLayoutManager(getActivity()));
        billingDetailsAdapter = new BillingDetailsAdapter(getActivity(), arList);
        rcBillingDetails.setAdapter(billingDetailsAdapter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
