package fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import adapters.TabPagerAdapter;
import util.Config;
import widgets.SlidingTabLayout;

/**
 * Created by SaraschandraaM on 14/01/16.
 */

@EFragment(R.layout.fragment_pager)
public class PagerFragment extends Fragment {

    boolean isTablet;
    TabPagerAdapter tabPagerAdapter;
    int PAGELIMIT_CONSTANT;

    @ViewById(R.id.st_accounttabs)
    TabLayout slidingTabs;

    @ViewById(R.id.pager)
    ViewPager tabPager;

    @ViewById(R.id.tv_more)
    TextView tvMore;


    @AfterViews
    void onViewLoaded() {
        if (isTablet) {
            PAGELIMIT_CONSTANT = 6;
        } else {
            PAGELIMIT_CONSTANT = 3;
        }
        tabPagerAdapter = new TabPagerAdapter(getActivity(), getChildFragmentManager(), AccountsHomeActivity.getInstance().accountsList, isTablet);
        tabPager.setAdapter(tabPagerAdapter);
        tabPager.setOffscreenPageLimit(PAGELIMIT_CONSTANT);
        slidingTabs.setupWithViewPager(tabPager);
        slidingTabs.setTabsFromPagerAdapter(tabPagerAdapter);
        slidingTabs.setBackgroundColor(getResources().getColor(R.color.color_tab));
        slidingTabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.color_blue));
        slidingTabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(tabPager));

        if (AccountsHomeActivity.getInstance().accountsList.getAccountsList().size() > 3) {
            tvMore.setVisibility(View.VISIBLE);
        } else {
            tvMore.setVisibility(View.GONE);
        }


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isTablet = Config.isTablet(getActivity());
    }

    @Override
    public void onResume() {
        super.onResume();
        AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(R.string.screen_ubacc);
        AccountsHomeActivity.getInstance().setNavMenuTextColor(0);
        onViewLoaded();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
