package fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import model.AccountDetails;
import model.Accounts;
import util.Config;

/**
 * Created by SaraschandraaM on 28/01/16.
 */
@EFragment(R.layout.fragment_makepayment)
public class MakePaymentFragment extends Fragment {

    @ViewById(R.id.tv_mkpay_amtopay)
    TextView mTvAmountToPay;

    @ViewById(R.id.tv_btn_shwpayacc)
    TextView mTvBtnShowPayAcc;

    public static enum MKPAY_FRAGMENTS {STEP_ONE, STEP_TWO, STEP_THREE, STEP_FOUR}

    public static MakePaymentFragment rootInstance;

    public static MakePaymentFragment getInstance() {
        return rootInstance;
    }

    ArrayList<AccountDetails> accountDetails;
    ArrayList<Accounts> selectedAccounts;
    String sTotalAmount;
    Bundle arguments;

    FragmentTransaction childFragmentTransaction;
    MakePaymentsFragments1 makePaymentsFragments1;
    MakePaymentsFragments2 makePaymentsFragments2;
    MakePaymentsFragments3 makePaymentsFragments3;
    MakePaymentsFragments4 makePaymentsFragments4;


    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        if (arguments != null) {
            accountDetails = (ArrayList<AccountDetails>) arguments.getSerializable("SelectedAccountDetails");
            selectedAccounts = (ArrayList<Accounts>) arguments.getSerializable("SelectedAccountList");
            sTotalAmount = arguments.getString("TotalAmount", "0.00");
        }

        mTvAmountToPay.setText(Config.formatCurrency(sTotalAmount));
        if (selectedAccounts.size() > 1) {
            mTvBtnShowPayAcc.setVisibility(View.VISIBLE);
        } else {
            mTvBtnShowPayAcc.setVisibility(View.GONE);
        }
        registerForContextMenu(mTvBtnShowPayAcc);
        changeMakePaymentFragments(MKPAY_FRAGMENTS.STEP_ONE, null);
    }

    @Click(R.id.tv_btn_shwpayacc)
    void onPayingAccountsClicked(View view) {
        this.getActivity().openContextMenu(view);
    }

    public void changeMakePaymentFragments(MKPAY_FRAGMENTS mkpay_fragments, Bundle args) {
        childFragmentTransaction = getChildFragmentManager().beginTransaction();
        switch (mkpay_fragments) {
            case STEP_ONE:
                makePaymentsFragments1 = new MakePaymentsFragments1_();
                try {
                    makePaymentsFragments1.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                childFragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
                childFragmentTransaction.replace(R.id.fl_mkpay_fragmentholder, makePaymentsFragments1);
                childFragmentTransaction.addToBackStack(null);
                childFragmentTransaction.commit();
                break;

            case STEP_TWO:
                makePaymentsFragments2 = new MakePaymentsFragments2_();
                try {
                    makePaymentsFragments2.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                childFragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
                childFragmentTransaction.replace(R.id.fl_mkpay_fragmentholder, makePaymentsFragments2);
                childFragmentTransaction.addToBackStack(null);
                childFragmentTransaction.commit();
                break;

            case STEP_THREE:
                makePaymentsFragments3 = new MakePaymentsFragments3_();
                try {
                    makePaymentsFragments3.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                childFragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
                childFragmentTransaction.replace(R.id.fl_mkpay_fragmentholder, makePaymentsFragments3);
                childFragmentTransaction.addToBackStack(null);
                childFragmentTransaction.commit();
                break;

            case STEP_FOUR:
                makePaymentsFragments4 = new MakePaymentsFragments4_();
                try {
                    makePaymentsFragments4.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                childFragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
                childFragmentTransaction.replace(R.id.fl_mkpay_fragmentholder, makePaymentsFragments4);
                childFragmentTransaction.addToBackStack(null);
                childFragmentTransaction.commit();
                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootInstance = this;
    }

    @Override
    public void onResume() {
        super.onResume();
        AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(getString(R.string.screen_mkpay));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        LayoutInflater headerInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        ViewGroup header = (ViewGroup) headerInflater.inflate(R.layout.cell_menuheader, null);

        // menu.setHeaderView(header);
        TextView title = (TextView) header.findViewById(R.id.tv_menuheader);
        title.setText("Paying Accounts");
        menu.setHeaderView(header);
        for (int i = 0; i < selectedAccounts.size(); i++) {
            menu.add(1, i, Menu.NONE, selectedAccounts.get(i).getAccountNumber());
        }
    }

}
