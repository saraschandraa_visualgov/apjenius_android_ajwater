package fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.AccountsHomeActivity.Fragments;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import adapters.BillingHistoryAdapter;
import adapters.ConsumptionHistoryAdapter;
import model.AccountDetails;
import model.Accounts;
import util.Config;

/**
 * Created by SaraschandraaM on 14/01/16.
 */

@EFragment(R.layout.fragment_accountoverview)
public class AccountOverviewFragment extends Fragment {

    @ViewById(R.id.tv_accover_accno)
    TextView tvAccountNumber;

    @ViewById(R.id.tv_accover_custname)
    TextView tvCustomerName;

    @ViewById(R.id.tv_accover_prsbal)
    TextView tvPreviousBalance;

    @ViewById(R.id.tv_accover_amtdue)
    TextView tvAmountDue;

    @ViewById(R.id.iv_accover_acinfo)
    ImageView ivAccInfoDetail;

    @ViewById(R.id.rc_consumptionhistory)
    RecyclerView rcConsumptionHistory;

    @ViewById(R.id.rc_accinfo_billinghistory)
    RecyclerView rcBillingHistory;


    AccountDetails accountDetails;
    Accounts accounts;

    Bundle arguments;

    int pagerPosition;

    ConsumptionHistoryAdapter consumptionHistoryAdapter;
    BillingHistoryAdapter billingHistoryAdapter;


    public static AccountOverviewFragment getInstance() {
        AccountOverviewFragment accountOverviewFragment = new AccountOverviewFragment_();
        return accountOverviewFragment;
    }

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        if (arguments != null) {
            accountDetails = (AccountDetails) arguments.getSerializable("AccountDetail");
            accounts = (Accounts) arguments.getSerializable("Account");
            pagerPosition = arguments.getInt("Position");
        }
        tvAccountNumber.setText(accounts.getAccountNumber());
        tvCustomerName.setText(accountDetails.getUbDetailsList().getName());
        tvPreviousBalance.setText(Config.formatCurrency(accountDetails.getUbDetailsList().getPreviousBalance()));
        tvAmountDue.setText(Config.formatCurrency(accountDetails.getUbDetailsList().getAmountDue()));
        rcConsumptionHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcBillingHistory.setLayoutManager(new LinearLayoutManager(getActivity()));

        consumptionHistoryAdapter = new ConsumptionHistoryAdapter(getActivity(), accountDetails.getWaterConses());
        rcConsumptionHistory.setAdapter(consumptionHistoryAdapter);

        rcBillingHistory.setAdapter(new BillingHistoryAdapter(getActivity(), accountDetails.getBillingHistories()));

    }


    @Click(R.id.iv_accover_acinfo)
    void onAccountInfoDetailClicked() {
        Bundle args = new Bundle();
        args.putInt("Position", pagerPosition);
        AccountsHomeActivity.getInstance().changeFragments(Fragments.PAGER_ACCOUNTINFO, args);
    }

    @Click(R.id.iv_accover_consuminfo)
    void onConsumptionHistoryInfoClicked() {
        if (accountDetails.getWaterConses() != null && accountDetails.getWaterConses().size() > 0) {
            Bundle args = new Bundle();
            args.putSerializable("WaterCons", accountDetails.getWaterConses());
            AccountsHomeActivity.getInstance().changeFragments(Fragments.INFO_CONSHIS, args);
        } else {
        }

    }

    @Click(R.id.iv_accover_billhisinfo)
    void onBillingHistoryInfoClicked() {
        if (accountDetails.getBillingHistories() != null && accountDetails.getBillingHistories().size() > 0) {
            Bundle args = new Bundle();
            args.putSerializable("BillingHistory", accountDetails.getBillingHistories());
            AccountsHomeActivity.getInstance().changeFragments(Fragments.INFO_BILLHIS, args);
        }
    }

    @Click(R.id.btn_accover_paynow)
    void onPayNowClicked() {
        AccountsHomeActivity.getInstance().selectedAccounts = new ArrayList<>();
        AccountsHomeActivity.getInstance().selectedAccountDetails = new ArrayList<>();
        AccountsHomeActivity.getInstance().selectedAccounts.add(accounts);
        AccountsHomeActivity.getInstance().selectedAccountDetails.add(accountDetails);
        Bundle args = new Bundle();
        args.putSerializable("SelectedAccountDetails", AccountsHomeActivity.getInstance().selectedAccountDetails);
        args.putSerializable("SelectedAccountList", AccountsHomeActivity.getInstance().selectedAccounts);
        args.putString("TotalAmount", accountDetails.getUbDetailsList().getAmountDue());
        AccountsHomeActivity.getInstance().changeFragments(Fragments.MAKE_PAYMENT, args);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(R.string.screen_ubacc);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
