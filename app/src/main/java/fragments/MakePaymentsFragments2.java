package fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.EditText;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

/**
 * Created by macuser on 1/27/16.
 */
@EFragment(R.layout.fragment_makepayment2)
public class MakePaymentsFragments2 extends Fragment {

    @ViewById(R.id.et_firstname)
    EditText etFirstName;

    @ViewById(R.id.et_lastname)
    EditText etLastName;

    @ViewById(R.id.et_streetaddress)
    EditText etStreetAddress;

    @ViewById(R.id.et_addressoptional)
    EditText etAddressOptional;

    @ViewById(R.id.et_city)
    EditText etCity;

    @ViewById(R.id.et_state)
    EditText etState;

    @ViewById(R.id.et_zip)
    EditText etZipcode;

    @ViewById(R.id.et_phnum)
    EditText etPhoneNumber;

    @ViewById(R.id.et_eaddress)
    EditText etEmail;

    @AfterViews
    void onViewLoaded() {

    }

    @Click(R.id.iv_step3)
    void onStep3Clicked() {
        MakePaymentFragment.getInstance().changeMakePaymentFragments(MakePaymentFragment.MKPAY_FRAGMENTS.STEP_THREE, null);
    }

    @Click(R.id.iv_close)
    void onCloseClicked(){
        AccountsHomeActivity.getInstance().onBackPressed();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
