package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.TextView;

import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import model.AccountDetails;
import model.Accounts;
import model.UBDetailsList;

/**
 * Created by SaraschandraaM on 19/01/16.
 */
@EFragment(R.layout.fragment_accountinfo)
public class AccountInfoFragment extends Fragment {


    @ViewById(R.id.tv_ai_accno)
    TextView tvAIAccNo;

    @ViewById(R.id.tv_ai_custname)
    TextView tvAICustName;

    @ViewById(R.id.tv_ai_addr1)
    TextView tvAIAddr1;

    @ViewById(R.id.tv_ai_addr2)
    TextView tvAIAddr2;

    @ViewById(R.id.tv_ai_srvaddr)
    TextView tvAIServiceAddr;

    @ViewById(R.id.tv_ai_nomtrs)
    TextView tvAINumMeters;

    @ViewById(R.id.tv_ai_homeno)
    TextView tvAIHomeNo;

    @ViewById(R.id.tv_ai_workno)
    TextView tvAIWorkNo;

    @ViewById(R.id.tv_ai_moveindate)
    TextView tvAIMoveDate;


    AccountDetails accountDetails;
    UBDetailsList ubDetailsList;
    Accounts accounts;
    Bundle arguments;


    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        if (arguments != null) {
            accountDetails = (AccountDetails) arguments.getSerializable("AccountDetail");
            accounts = (Accounts) arguments.getSerializable("Account");
            ubDetailsList = accountDetails.getUbDetailsList();
        }

        tvAIAccNo.setText(accounts.getAccountNumber());
        tvAICustName.setText(ubDetailsList.getName());
        tvAIAddr1.setText(ubDetailsList.getAddress1());
        tvAIAddr2.setText(ubDetailsList.getAddress2());
        tvAIServiceAddr.setText(ubDetailsList.getServiceAddress());
        tvAINumMeters.setText(ubDetailsList.getNumberOfMeters());
        tvAIHomeNo.setText(ubDetailsList.getHomeNumber());
        tvAIWorkNo.setText(ubDetailsList.getWorkNumber());
        tvAIMoveDate.setText(ubDetailsList.getMoveInDate());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
