package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.AccountsHomeActivity.Fragments;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import adapters.ChoosePaymentAdapter;
import model.AccountDetails;
import model.AccountsList;
import util.Config;

/**
 * Created by SaraschandraaM on 28/01/16.
 */
@EFragment(R.layout.fragment_choosepayment)
public class ChoosePaymentFragment extends Fragment implements ChoosePaymentAdapter.OnPaymentListClickListener {

    @ViewById(R.id.rc_choosepayment)
    RecyclerView mRcChoosePayment;

    @ViewById(R.id.tv_mkpay_totamount)
    TextView mTvTotalAmount;

    Bundle arguments;
    ArrayList<AccountDetails> accountDetailsList;
    AccountsList accountsList;
    String sTotalAmount;

    ChoosePaymentAdapter choosePaymentAdapter;


    @AfterViews
    void onViewLoaded() {
        mRcChoosePayment.setLayoutManager(new LinearLayoutManager(getActivity()));
        AccountsHomeActivity.getInstance().selectedAccountDetails = new ArrayList<>();
        AccountsHomeActivity.getInstance().selectedAccounts = new ArrayList<>();
        arguments = getArguments();

        if (arguments != null) {
            accountDetailsList = (ArrayList<AccountDetails>) arguments.getSerializable("AccountDetail");
            accountsList = (AccountsList) arguments.getSerializable("AccountList");
        }

        choosePaymentAdapter = new ChoosePaymentAdapter(getActivity(), accountDetailsList, accountsList, Config.isTab, false);
        choosePaymentAdapter.setOnPaymentListClickListener(this);
        mRcChoosePayment.setAdapter(choosePaymentAdapter);
    }

    private void setTotalAmount() {
        if (AccountsHomeActivity.getInstance().selectedAccountDetails != null && AccountsHomeActivity.getInstance().selectedAccountDetails.size() > 0) {
            double amount = 0.00;
            for (int i = 0; i < AccountsHomeActivity.getInstance().selectedAccountDetails.size(); i++) {
                amount = amount + Double.parseDouble(AccountsHomeActivity.getInstance().selectedAccountDetails.get(i).getUbDetailsList().getAmountDue());
            }
            sTotalAmount = "" + amount;
            mTvTotalAmount.setText(Config.formatCurrency(sTotalAmount));
        }
    }

    @Click(R.id.tv_btnmk_paynow)
    void onPayNowClicked() {
        AccountsHomeActivity.getInstance().onBackPressed();
        Bundle args = new Bundle();
        args.putSerializable("SelectedAccountDetails", AccountsHomeActivity.getInstance().selectedAccountDetails);
        args.putSerializable("SelectedAccountList", AccountsHomeActivity.getInstance().selectedAccounts);
        args.putString("TotalAmount", sTotalAmount);
        AccountsHomeActivity.getInstance().changeFragments(Fragments.MAKE_PAYMENT, args);
    }

    @Click(R.id.tv_btnmk_cancel)
    void onCancelClicked() {
        AccountsHomeActivity.getInstance().onBackPressed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!Config.isTab) {
            AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(getString(R.string.screen_mkpay));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPaymentListClicked(View view, int position) {
        choosePaymentAdapter.setSelectedPosition(position);
        choosePaymentAdapter.notifyDataSetChanged();
        setTotalAmount();
    }
}
