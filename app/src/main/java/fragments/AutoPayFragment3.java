package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import adapters.ChoosePaymentAdapter;
import model.AccountDetails;
import model.AccountsList;
import util.Config;

/**
 * Created by SaraschandraaM on 29/01/16.
 */
@EFragment(R.layout.fragment_autopay3)
public class AutoPayFragment3 extends Fragment {

    @ViewById(R.id.rc_autopay)
    RecyclerView mRcAutoPayList;

    Bundle arguments;
    ArrayList<AccountDetails> accountDetailsList;
    AccountsList accountsList;

    ChoosePaymentAdapter choosePaymentAdapter;

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        if (!Config.isTab) {
            mRcAutoPayList.setLayoutManager(new LinearLayoutManager(getActivity()));
        } else {
            mRcAutoPayList.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        }
        if (arguments != null) {
            accountDetailsList = (ArrayList<AccountDetails>) arguments.getSerializable("AccountDetail");
            accountsList = (AccountsList) arguments.getSerializable("AccountList");
        }
        choosePaymentAdapter = new ChoosePaymentAdapter(getActivity(), accountDetailsList, accountsList, Config.isTab, true);
        mRcAutoPayList.setAdapter(choosePaymentAdapter);
    }

    @Click(R.id.iv_autopay_step4)
    void onStep4Clicked() {
        AccountsHomeActivity.getInstance().changeFragments(AccountsHomeActivity.Fragments.AUTOPAY_4, null);
    }

    @Click(R.id.iv_close)
    void onCloseClicked() {
        AccountsHomeActivity.getInstance().homeButtonPressed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(getString(R.string.screen_autopay));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
