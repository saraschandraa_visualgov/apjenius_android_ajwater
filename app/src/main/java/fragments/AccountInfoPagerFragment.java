package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import adapters.AccountInfoDetailPagerAdapter;
import adapters.TabPagerAdapter;
import widgets.SlidingTabLayout;

/**
 * Created by SaraschandraaM on 19/01/16.
 */

@EFragment(R.layout.fragment_accountinfopager)
public class AccountInfoPagerFragment extends Fragment {

    @ViewById(R.id.st_accountinfotabs)
    TabLayout accountInfoSlidingTabs;

    @ViewById(R.id.accountinfopager)
    ViewPager accountInfoPager;

    Bundle arguments;

    int pagerPosition;
    AccountInfoDetailPagerAdapter accountInfoPagerAdapter;

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        if (arguments != null) {
            pagerPosition = arguments.getInt("Position", 0);
        }
        accountInfoPagerAdapter = new AccountInfoDetailPagerAdapter(getActivity(), getChildFragmentManager(), pagerPosition);
        accountInfoPager.setAdapter(accountInfoPagerAdapter);
        accountInfoPager.setOffscreenPageLimit(3);
        accountInfoSlidingTabs.setupWithViewPager(accountInfoPager);
        accountInfoSlidingTabs.setTabsFromPagerAdapter(accountInfoPagerAdapter);
        accountInfoSlidingTabs.setBackgroundColor(getResources().getColor(R.color.color_tab));
        accountInfoSlidingTabs.setSelectedTabIndicatorColor(getResources().getColor(R.color.color_blue));
        accountInfoSlidingTabs.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(accountInfoPager));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
