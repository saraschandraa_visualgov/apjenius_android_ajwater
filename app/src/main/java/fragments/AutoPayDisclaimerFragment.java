package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

import java.util.ArrayList;

import model.AccountDetails;
import model.AccountsList;

/**
 * Created by SaraschandraaM on 29/01/16.
 */
@EFragment(R.layout.fragment_autopaydisclaimer)
public class AutoPayDisclaimerFragment extends Fragment {


    Bundle arguments;
    ArrayList<AccountDetails> accountDetailsList;
    AccountsList accountsList;


    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        if (arguments != null) {
            accountDetailsList = (ArrayList<AccountDetails>) arguments.getSerializable("AccountDetail");
            accountsList = (AccountsList) arguments.getSerializable("AccountList");
        }
    }


    @Click(R.id.tv_btnautopay_accept)
    void onAutoPayAcceptClicked() {
        AccountsHomeActivity.getInstance().onBackPressed();
        Bundle args = new Bundle();
        args.putSerializable("AccountDetail", accountDetailsList);
        args.putSerializable("AccountList", accountsList);
        AccountsHomeActivity.getInstance().changeFragments(AccountsHomeActivity.Fragments.AUT0PAY_1, args);
    }

    @Click(R.id.tv_btnautopay_decline)
    void onAutoPayDeclined() {
        AccountsHomeActivity.getInstance().onBackPressed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
