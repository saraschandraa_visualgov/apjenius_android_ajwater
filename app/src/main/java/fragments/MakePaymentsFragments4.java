package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

/**
 * Created by macuser on 1/27/16.
 */
@EFragment(R.layout.fragment_makepayment4)
public class MakePaymentsFragments4 extends Fragment {


    @AfterViews
    void onViewLoaded() {

    }

    @Click(R.id.iv_mkpay_done)
    void onMakePaymentDone() {
        AccountsHomeActivity.getInstance().onBackPressed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
