package fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

/**
 * Created by macuser on 1/27/16.
 */
@EFragment(R.layout.fragment_makepayment1)
public class MakePaymentsFragments1 extends Fragment {


    @AfterViews
    void onViewLoaded() {

    }

    @Click(R.id.rl_creditcard)
    void onCreditCardClicked() {
        MakePaymentFragment.getInstance().changeMakePaymentFragments(MakePaymentFragment.MKPAY_FRAGMENTS.STEP_TWO, null);
    }

    @Click(R.id.rl_echeck)
    void onECheckClicked() {
        MakePaymentFragment.getInstance().changeMakePaymentFragments(MakePaymentFragment.MKPAY_FRAGMENTS.STEP_TWO, null);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(getString(R.string.screen_mkpay));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

}
