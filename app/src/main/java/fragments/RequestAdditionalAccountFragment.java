package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;

import util.Config;

/**
 * Created by SaraschandraaM on 29/01/16.
 */
@EFragment(R.layout.fragment_requestaddtionalaccount)
public class RequestAdditionalAccountFragment extends Fragment {

    @AfterViews
    void onViewLoaded() {

    }


    @Click(R.id.btn_req_submit)
    void onSubmitClicked() {
        AccountsHomeActivity.getInstance().onBackPressed();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!Config.isTab) {
            AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(getString(R.string.screen_reqacc));
        }
    }
}
