package fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.EditText;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import model.AccountDetails;
import model.AccountsList;

/**
 * Created by macuser on 1/27/16.
 */
@EFragment(R.layout.fragment_autopay1)

public class AutoPayFragment1 extends Fragment {

    @ViewById(R.id.et_firstname)
    EditText etFirstname;

    @ViewById(R.id.et_lastname)
    EditText etLastname;

    @ViewById(R.id.et_staddress)
    EditText etStreetaddress;

    @ViewById(R.id.et_addressoptional)
    EditText etAddressoptional;

    @ViewById(R.id.et_city)
    EditText etCity;

    @ViewById(R.id.et_state)
    EditText etState;

    @ViewById(R.id.et_zipcode)
    EditText etZip;

    @ViewById(R.id.et_phnum)
    EditText etPhonenum;

    @ViewById(R.id.et_eaddress)
    EditText etEmail;


    Bundle arguments;
    ArrayList<AccountDetails> accountDetailsList;
    AccountsList accountsList;

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        if (arguments != null) {
            accountDetailsList = (ArrayList<AccountDetails>) arguments.getSerializable("AccountDetail");
            accountsList = (AccountsList) arguments.getSerializable("AccountList");
        }
    }

    @Click(R.id.iv_autopay_step2)
    void onStep2Clicked() {
        Bundle args = new Bundle();
        args.putSerializable("AccountDetail", accountDetailsList);
        args.putSerializable("AccountList", accountsList);
        AccountsHomeActivity.getInstance().changeFragments(AccountsHomeActivity.Fragments.AUTOPAY_2, args);
    }

    @Click(R.id.iv_close)
    void onCloseClicked() {
        AccountsHomeActivity.getInstance().homeButtonPressed();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(getString(R.string.screen_autopay));
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
