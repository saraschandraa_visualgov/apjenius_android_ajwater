package fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.widget.EditText;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import model.AccountDetails;
import model.AccountsList;

/**
 * Created by macuser on 1/27/16.
 */
@EFragment(R.layout.fragment_autopay2)
public class AutoPayFragment2 extends Fragment {

    @ViewById(R.id.et_cardnum)
    EditText etCardNum;

    @ViewById(R.id.et_cardmonth)
    EditText etCardMonth;

    @ViewById(R.id.et_cardyear)
    EditText etCardYear;

    Bundle arguments;
    ArrayList<AccountDetails> accountDetailsList;
    AccountsList accountsList;

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        if (arguments != null) {
            accountDetailsList = (ArrayList<AccountDetails>) arguments.getSerializable("AccountDetail");
            accountsList = (AccountsList) arguments.getSerializable("AccountList");
        }
    }

    @Click(R.id.iv_autopay_step3)
    void onStep3Clicked() {
        Bundle args = new Bundle();
        args.putSerializable("AccountDetail", accountDetailsList);
        args.putSerializable("AccountList", accountsList);
        AccountsHomeActivity.getInstance().changeFragments(AccountsHomeActivity.Fragments.AUTOPAY_3, args);
    }

    @Click(R.id.iv_close)
    void onCloseClicked() {
        AccountsHomeActivity.getInstance().homeButtonPressed();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(getString(R.string.screen_autopay));
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
