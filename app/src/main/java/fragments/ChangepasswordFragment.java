package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.EditText;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import util.Config;

/**
 * Created by macuser on 1/27/16.
 */
@EFragment(R.layout.fragment_changepassword)
public class ChangepasswordFragment extends Fragment {


    @ViewById(R.id.et_oldpswd)
    EditText etOldPassword;

    @ViewById(R.id.et_newpswd)
    EditText etNewPassword;

    @ViewById(R.id.et_confirmpswd)
    EditText etConfirmPassword;


    @AfterViews
    void onViewLoaded() {

    }

    @Click(R.id.et_submit)
    void onSubmitClicked() {
        AccountsHomeActivity.getInstance().onBackPressed();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!Config.isTab) {
            AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(getString(R.string.screen_password));
        }
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
