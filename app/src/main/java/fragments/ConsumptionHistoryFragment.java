package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import adapters.ConsumptionHistoryAdapter;
import model.WaterCons;
import util.Config;

/**
 * Created by SaraschandraaM on 21/01/16.
 */
@EFragment(R.layout.fragment_consumptionhistorydetail)
public class ConsumptionHistoryFragment extends Fragment {


    @ViewById(R.id.rc_consumptionhistorydetail)
    RecyclerView rcConsumptionHistoryDetail;

    Bundle arguments;
    ArrayList<WaterCons> waterConsArrayList;

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        rcConsumptionHistoryDetail.setLayoutManager(new LinearLayoutManager(getActivity()));
        if (arguments != null) {
            waterConsArrayList = (ArrayList<WaterCons>) arguments.getSerializable("WaterCons");
        }

        if (waterConsArrayList != null) {
            rcConsumptionHistoryDetail.setAdapter(new ConsumptionHistoryAdapter(getActivity(), waterConsArrayList));
        }

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!Config.isTab) {
            AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(getString(R.string.screen_conshis));
        }
    }
}
