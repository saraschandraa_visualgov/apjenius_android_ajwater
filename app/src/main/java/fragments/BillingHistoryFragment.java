package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import adapters.BillingHistoryAdapter;
import model.BillingHistory;
import util.Config;

/**
 * Created by SaraschandraaM on 21/01/16.
 */
@EFragment(R.layout.fragment_billinghistory)
public class BillingHistoryFragment extends Fragment {

    @ViewById(R.id.rc_billinghistory)
    RecyclerView rcBillingHistory;

    Bundle arguments;
    ArrayList<BillingHistory> billingHistoryList;

    @AfterViews
    void onViewLoaded() {
        rcBillingHistory.setLayoutManager(new LinearLayoutManager(getActivity()));
        arguments = getArguments();

        if (arguments != null) {
            billingHistoryList = (ArrayList<BillingHistory>) arguments.getSerializable("BillingHistory");
        }

        if (billingHistoryList != null) {
            rcBillingHistory.setAdapter(new BillingHistoryAdapter(getActivity(), billingHistoryList));
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!Config.isTab){
            AccountsHomeActivity.getInstance().mTvToolbarTitle.setText(getString(R.string.screen_billhis));
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
