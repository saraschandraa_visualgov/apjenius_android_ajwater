package fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.TextView;

import com.visualgov.ajwater.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import model.AccountDetails;
import model.Accounts;
import model.UBDetailsList;

/**
 * Created by SaraschandraaM on 19/01/16.
 */
@EFragment(R.layout.fragment_balanceinfo)
public class BalanceInfoFragment extends Fragment {


    @ViewById(R.id.tv_bi_previousbal)
    TextView tvBIPreviousBal;

    @ViewById(R.id.tv_bi_ptdpay)
    TextView tvBIPTDPayments;

    @ViewById(R.id.tv_bi_ptdpenalties)
    TextView tvBIPTDPenalties;

    @ViewById(R.id.tv_bi_ptdadj)
    TextView tvBIPTDAdjustments;

    @ViewById(R.id.tv_bi_pendingpay)
    TextView tvBIPendingPayments;

    @ViewById(R.id.tv_bi_amountdue)
    TextView tvBIAmountDue;

    @ViewById(R.id.tv_bi_currentamt)
    TextView tvBiCurrentAmount;

    @ViewById(R.id.tv_bi_over30)
    TextView tvBIOver30;

    @ViewById(R.id.tv_bi_over60)
    TextView tvBIOver60;

    @ViewById(R.id.tv_bi_over90)
    TextView tvBIOver90;

    @ViewById(R.id.tv_bi_over120)
    TextView tvBIOver120;


    AccountDetails accountDetails;
    UBDetailsList ubDetailsList;
    Accounts accounts;
    Bundle arguments;

    @AfterViews
    void onViewLoaded() {
        arguments = getArguments();
        if (arguments != null) {
            accountDetails = (AccountDetails) arguments.getSerializable("AccountDetail");
            accounts = (Accounts) arguments.getSerializable("Account");
            ubDetailsList = accountDetails.getUbDetailsList();
        }
        tvBIPreviousBal.setText(ubDetailsList.getPreviousBalance());
        tvBIPTDPayments.setText(ubDetailsList.getPTDPayments());
        tvBIPTDPenalties.setText(ubDetailsList.getPTDPenalties());
        tvBIPTDAdjustments.setText(ubDetailsList.getPTDAdjustment());
        tvBIPendingPayments.setText(ubDetailsList.getPendingPayments());
        tvBIAmountDue.setText(ubDetailsList.getAmountDue());
        tvBiCurrentAmount.setText(ubDetailsList.getCurrentAmount());
        tvBIOver30.setText(ubDetailsList.getOver30Days());
        tvBIOver60.setText(ubDetailsList.getOver60Days());
        tvBIOver90.setText(ubDetailsList.getOver90Days());
        tvBIOver120.setText(ubDetailsList.getOver120Days());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
