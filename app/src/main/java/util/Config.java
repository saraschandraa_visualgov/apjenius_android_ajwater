package util;

import android.content.Context;

import com.visualgov.ajwater.R;

import java.text.DecimalFormat;

/**
 * Created by SaraschandraaM on 05/01/16.
 */
public class Config {


    public static boolean isTab;

    public static boolean isTablet(Context context) {
        return context.getResources().getBoolean(R.bool.isTablet);
    }

    /**
     * URLs
     **/
    public static String URL_LOGIN = "https://mobiledata.visualgov.com/UtilityAPIVN/UtilDetail/GetUserInfo";
    public static String URL_GETACCOUNT = "https://mobiledata.visualgov.com/UtilityAPIVN/UtilDetail/GetAccounts";
    public static String URL_GETACCOUNTDETAILS = "https://mobiledata.visualgov.com/UtilityAPIVN/UtilDetail/GetUBAccountInfo";


    /**
     * Constants
     **/
    public static String VGToken = "VGVN983719";


    /**
     * Sample Response
     **/
    public static String ACCOUNTLIST = "{ \"Util\": { \"Accounts\": [ {\"AccountAddress\": \"504 Stacie Dr.\", \"AccountNumber\": \"22500200001\" }, {\"AccountAddress\": \"1022 NIAGARA RD.\", \"AccountNumber\": \"11412100011\" }, {\"AccountAddress\": \"1022 NIAGARA RD.\", \"AccountNumber\": \"11412100011\" }, {\"AccountAddress\": \"1022 NIAGARA RD.\", \"AccountNumber\": \"11412100011\" } ] }}";


    public static String formatCurrency(String data) {
        String amount = "";
        data = data.replaceAll("\\$", "");
        data = data.replaceAll("\\,", "");
        if (!data.equals("") || data != null || !data.equals("null")) {
            if (data.equals("0") || data.equals("0.00") || data.equals("0.0")) {
                data = "$0.00";
            } else {
                double voltax = Double.parseDouble(data);
                DecimalFormat formatter = new DecimalFormat("#,###,###.00");
                amount = formatter.format(voltax);
                data = "$" + amount;
            }
        }
        return data;
    }
}
