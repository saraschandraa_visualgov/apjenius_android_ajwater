package util;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by SaraschandraaM on 27/10/15.
 */
public class HTTPSConnectionHandler {


    public static HTTPSConnectionHandler httpHandler = null;

    Context context;

    public HTTPSConnectionHandler(Context context) {
        this.context = context;
    }

    public static HTTPSConnectionHandler defaultHandler(Context context) {
        if (httpHandler == null) {
            httpHandler = new HTTPSConnectionHandler(context);
        }
        return httpHandler;
    }

    public String doPost(String url, JSONObject params) {

        String json = "";
        InputStream is = null;
        StringBuffer response = new StringBuffer();
        try {
            URL obj = new URL(url);

            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();

            /** add reuqest header **/
            con.setRequestMethod("POST");
            con.setDoOutput(true);
            con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded ");
//            con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
            if (Build.VERSION.SDK != null && Build.VERSION.SDK_INT > 13) {
                con.setRequestProperty("Connection", "close");
            }

            String urlParameters = "";


            // Send post request
            con.setDoOutput(true);
            Uri.Builder urlBuilder = new Uri.Builder();
            OutputStream os = con.getOutputStream();
            BufferedWriter wr = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            try {
                if (params != null) {

                    Map<String, Object> retMap = new HashMap<String, Object>();

                    retMap = toMap(params);

                    StringBuilder paramspost = new StringBuilder();
                    boolean first = true;
                    for (Map.Entry<String, Object> entry : retMap.entrySet()) {
                        if (first)
                            first = false;
                        else
                            paramspost.append("&");

//                        paramspost.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
//                        paramspost.append("=");
//                        paramspost.append(URLEncoder.encode(entry.getValue().toString(), "UTF-8"));
                        urlBuilder.appendQueryParameter(entry.getKey(), entry.getValue().toString());
                    }
                    urlParameters = urlBuilder.build().getEncodedQuery();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            wr.write(urlParameters);
            wr.flush();
            wr.close();
            os.close();

            int responseCode = con.getResponseCode();
//            System.out.println("\nSending 'POST' request to URL : " + url);
//            System.out.println("Post parameters : " + urlParameters);
//            System.out.println("Response Code : " + responseCode);
            Log.i("POST URL", url);
            Log.i("POST PARAMS" , ""+params);

            BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            json = response.toString();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return json;
    }

    public String doGet(String url, JSONObject params) {

        String json = "";
        InputStream is = null;
        HttpsURLConnection urlConnection = null;
        StringBuilder result = new StringBuilder();

        try {
            if (params != null) {

                Map<String, Object> retMap = new HashMap<String, Object>();

                retMap = toMap(params);
                StringBuilder paramspost = new StringBuilder();
                boolean first = true;
                for (Map.Entry<String, Object> entry : retMap.entrySet()) {
                    if (first)
                        first = false;
                    else
                        paramspost.append("&");

                    paramspost.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
                    paramspost.append("=");
                    paramspost.append(URLEncoder.encode(entry.getValue().toString(), "UTF-8"));
                }
                url = url + "?" + paramspost.toString();
            }

            URL getURL = new URL(url);
            urlConnection = (HttpsURLConnection) getURL.openConnection();
            urlConnection.addRequestProperty("Cache-Control", "max-age=0");
            urlConnection.setUseCaches(true);
            urlConnection.setConnectTimeout(10000);
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());


            Log.i("GET URL", url);

            BufferedReader reader = new BufferedReader(new InputStreamReader(in));

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }
            json = result.toString();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            urlConnection.disconnect();
        }
        return json;
    }

    public Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    public List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }
}

