package util;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import listener.WSListener;


/**
 * Created by SaraschandraaM on 18/08/15.
 */
public class WebServiceUtil extends AsyncTask<String, Void, String> {

    String url;
    Context context;
    WSListener listener;
    Exception exception;
    String response = null;
    JSONObject param = new JSONObject();
    HTTPSConnectionHandler handler;
    WSTYPE webserviceType;

    public static enum WSTYPE{
        DO_POST, DO_GET
    }

    public WebServiceUtil(Context context, String url, JSONObject params, WSListener listener, WSTYPE webserviceType){
        this.url = url;
        this.param = params;
        this.context = context;
        this.listener = listener;
        this.webserviceType = webserviceType;
    }


    @Override
    protected String doInBackground(String... params) {
        handler = new HTTPSConnectionHandler(context);

        try{
            switch (webserviceType){
                case DO_GET:
                    response = handler.doGet(url, param);
                    Log.i("WS GET Response" ,response);
                    break;

                case DO_POST:
                    response = handler.doPost(url, param);
                    Log.i("WS POST Response" ,response);
                    break;
            }


            return response;
        }catch (Exception e){
            Log.e("WS Exception", e.getMessage());
            exception = e;
        }
        return response;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if(listener != null){
            listener.onRequestSent();
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if(listener != null){
            listener.onRequestReceived(s, exception);
        }
    }
}
