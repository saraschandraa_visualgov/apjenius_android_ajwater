package util;

import android.content.Context;

import org.json.JSONObject;

import listener.WSListener;

/**
 * Created by SaraschandraaM on 13/01/16.
 */
public class WebServiceCall {

    Context context;
    WebServiceUtil webServiceUtil;

    public WebServiceCall(Context context) {
        this.context = context;
    }


    public void doPostMethod(String URL, JSONObject params, WebServiceUtil.WSTYPE webserviceType, WSListener wsListener) {
        webServiceUtil = new WebServiceUtil(context, URL, params, wsListener, webserviceType);
        webServiceUtil.execute();
    }

    public void doGetMethod(String URL, JSONObject params, WebServiceUtil.WSTYPE webserviceType, WSListener wsListener) {
        webServiceUtil = new WebServiceUtil(context, URL, params, wsListener, webserviceType);
        webServiceUtil.execute();
    }
}
