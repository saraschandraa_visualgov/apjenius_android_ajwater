package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.visualgov.ajwater.R;

import java.util.ArrayList;

import model.BillingHistory;
import util.Config;

/**
 * Created by kandan on 1/20/2016.
 */
public class BillingHistoryAdapter extends RecyclerView.Adapter<BillingHistoryAdapter.BillingHistoryHolder> {

    Context context;
    ArrayList<BillingHistory> billingHistories;

    public BillingHistoryAdapter(Context context, ArrayList<BillingHistory> billingHistories) {

        this.context = context;
        this.billingHistories = billingHistories;

    }

    @Override
    public BillingHistoryHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View billingHistoryView = LayoutInflater.from(context).inflate(R.layout.cell_billinghistory, viewGroup, false);
        BillingHistoryHolder holder = new BillingHistoryHolder(billingHistoryView);
        return holder;
    }

    @Override
    public void onBindViewHolder(BillingHistoryHolder viewHolder, int position) {
        BillingHistory data = billingHistories.get(position);
        viewHolder.bTvDate.setText(data.getTransDate());
        viewHolder.bTVTransType.setText(data.getTransType());
        viewHolder.bTvAmount.setText(Config.formatCurrency(data.getAmount()));
        viewHolder.bTvBalance.setText(Config.formatCurrency(data.getRunningBalance()));

    }

    @Override
    public int getItemCount() {
        return billingHistories.size();
    }

    class BillingHistoryHolder extends RecyclerView.ViewHolder {

        TextView bTvDate, bTVTransType, bTvAmount, bTvBalance;

        public BillingHistoryHolder(View itemView) {

            super(itemView);

            bTvDate = (TextView) itemView.findViewById(R.id.tv_billingdate);
            bTVTransType = (TextView) itemView.findViewById(R.id.tv_billingtranstype);
            bTvAmount = (TextView) itemView.findViewById(R.id.tv_billingamount);
            bTvBalance = (TextView) itemView.findViewById(R.id.tv_billingbalance);


        }
    }

}
