package adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import fragments.AccountInfoFragment;
import fragments.AccountInfoFragment_;
import fragments.BalanceInfoFragment_;
import fragments.BillingDetailsFragment_;

/**
 * Created by SaraschandraaM on 19/01/16.
 */
public class AccountInfoDetailPagerAdapter extends FragmentPagerAdapter {

    Context context;
    String[] tabs;
    int pagerPositon;
    String tabHeader;
    Bundle args;

    public AccountInfoDetailPagerAdapter(Context context, FragmentManager fm, int pagerPostion) {
        super(fm);
        this.context = context;
        this.pagerPositon = pagerPostion;
        tabs = context.getResources().getStringArray(R.array.accinfo_pagertitle);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new AccountInfoFragment_();
                args = new Bundle();
                args.putSerializable("Account", AccountsHomeActivity.getInstance().accountsList.getAccountsList().get(pagerPositon));
                args.putSerializable("AccountDetail", AccountsHomeActivity.getInstance().accountDetails.get(pagerPositon));
                fragment.setArguments(args);
                break;

            case 1:
                fragment = new BalanceInfoFragment_();
                args = new Bundle();
                args.putSerializable("Account", AccountsHomeActivity.getInstance().accountsList.getAccountsList().get(pagerPositon));
                args.putSerializable("AccountDetail", AccountsHomeActivity.getInstance().accountDetails.get(pagerPositon));
                fragment.setArguments(args);
                break;

            case 2:
                fragment = new BillingDetailsFragment_();
                args = new Bundle();
                args.putSerializable("Account", AccountsHomeActivity.getInstance().accountsList.getAccountsList().get(pagerPositon));
                args.putSerializable("AccountDetail", AccountsHomeActivity.getInstance().accountDetails.get(pagerPositon));
                fragment.setArguments(args);
                break;
        }

        return fragment;
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getCount() {

        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        tabHeader = tabs[position];
        return tabHeader;
    }
}
