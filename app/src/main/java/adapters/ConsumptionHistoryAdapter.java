package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.visualgov.ajwater.R;

import java.util.ArrayList;

import model.WaterCons;

/**
 * Created by kandan on 1/20/2016.
 */
public class ConsumptionHistoryAdapter extends RecyclerView.Adapter<ConsumptionHistoryAdapter.ConsumptionHistoryHolder> {

    Context context;
    ArrayList<WaterCons> waterConses;

    public ConsumptionHistoryAdapter(Context context, ArrayList<WaterCons> waterConses) {
        this.context = context;
        this.waterConses = waterConses;
    }


    @Override
    public ConsumptionHistoryHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View consumptionHistoryView = LayoutInflater.from(context).inflate(R.layout.cell_consumptionhistory, viewGroup, false);
        ConsumptionHistoryHolder holder = new ConsumptionHistoryHolder(consumptionHistoryView);
        return holder;
    }

    @Override
    public void onBindViewHolder(ConsumptionHistoryHolder viewHolder, int position) {
        WaterCons data = waterConses.get(position);
        viewHolder.cTvDate.setText(data.getDate());
        viewHolder.cTvReading.setText((data.getReading()));
        viewHolder.cTvConsumption.setText(data.getCons());
        viewHolder.cTvType.setText(data.getType());

    }

    @Override
    public int getItemCount() {
        return waterConses.size();
    }

    class ConsumptionHistoryHolder extends RecyclerView.ViewHolder {

        TextView cTvDate, cTvReading, cTvConsumption, cTvType;

        public ConsumptionHistoryHolder(View itemView) {

            super(itemView);

            cTvDate = (TextView) itemView.findViewById(R.id.tv_date);
            cTvReading = (TextView) itemView.findViewById(R.id.tv_Reading);
            cTvConsumption = (TextView) itemView.findViewById(R.id.tv_consumption);
            cTvType = (TextView) itemView.findViewById(R.id.tv_type);

        }


    }
}
