package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.visualgov.ajwater.AccountsHomeActivity;
import com.visualgov.ajwater.R;

import java.util.ArrayList;

import model.AccountDetails;
import model.Accounts;
import model.AccountsList;
import util.Config;

/**
 * Created by SaraschandraaM on 28/01/16.
 */
public class ChoosePaymentAdapter extends RecyclerView.Adapter<ChoosePaymentAdapter.ChoosePaymentHolder> {

    Context context;
    ArrayList<AccountDetails> accountDetailsList;
    AccountsList accountsList;
    boolean isTab, isAutoPay;
    OnPaymentListClickListener onPaymentListClickListener;
    int selectedPosition = -1;

    public ChoosePaymentAdapter(Context context, ArrayList<AccountDetails> accountDetailsList, AccountsList accountList, boolean isTab, boolean isAutoPay) {
        this.context = context;
        this.accountDetailsList = accountDetailsList;
        this.accountsList = accountList;
        this.isTab = isTab;
        this.isAutoPay = isAutoPay;
    }

    public void setOnPaymentListClickListener(OnPaymentListClickListener onPaymentListClickListener) {
        this.onPaymentListClickListener = onPaymentListClickListener;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    @Override
    public ChoosePaymentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.cell_makepayment, parent, false);
        ChoosePaymentHolder holder = new ChoosePaymentHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ChoosePaymentHolder holder, final int position) {
        holder.amountToPay.setText(Config.formatCurrency(accountDetailsList.get(position).getUbDetailsList().getAmountDue()));
        holder.accountNo.setText(accountsList.getAccountsList().get(position).getAccountNumber());
        if (isTab) {
            if (!isAutoPay) {
                holder.servAddr.setText(accountDetailsList.get(position).getUbDetailsList().getServiceAddress());
            } else {
                holder.servAddr.setVisibility(View.GONE);
            }
        }

        if (isAutoPay) {
            holder.paymentSelector.setChecked(true);
            if (Config.isTab) {
                holder.paymentImage.setImageResource(R.drawable.ic_mkpay_check);
            }
        }
        holder.paymentSelector.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    if (isTab) {
                        holder.paymentImage.setImageResource(R.drawable.ic_mkpay_check);
                    }
                    AccountsHomeActivity.getInstance().selectedAccounts.add(accountsList.getAccountsList().get(position));
                    AccountsHomeActivity.getInstance().selectedAccountDetails.add(accountDetailsList.get(position));
                } else {
                    if (isTab) {
                        holder.paymentImage.setImageResource(R.drawable.ic_mkpay_uncheck);
                    }
                    AccountsHomeActivity.getInstance().selectedAccounts.remove(accountsList.getAccountsList().get(position));
                    AccountsHomeActivity.getInstance().selectedAccountDetails.remove(accountDetailsList.get(position));
                }
            }
        });

        if (selectedPosition == position) {
            if (holder.paymentSelector.isChecked()) {
                holder.paymentSelector.setChecked(false);
            } else {
                holder.paymentSelector.setChecked(true);
            }
        } else {
            if (holder.paymentSelector.isChecked()) {
                holder.paymentSelector.setChecked(true);
            } else {
                holder.paymentSelector.setChecked(false);
            }
        }
    }

    @Override
    public int getItemCount() {
        return accountsList.getAccountsList().size();
    }

    class ChoosePaymentHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView accountNo, amountToPay, servAddr;
        CheckBox paymentSelector;
        ImageView paymentImage;

        public ChoosePaymentHolder(View itemView) {
            super(itemView);
            accountNo = (TextView) itemView.findViewById(R.id.tv_mkpay_accno);
            amountToPay = (TextView) itemView.findViewById(R.id.tv_mkpay_amount);
            paymentSelector = (CheckBox) itemView.findViewById(R.id.ch_paymentselector);
            if (Config.isTab) {
                servAddr = (TextView) itemView.findViewById(R.id.tv_mkpay_servaddr);
                paymentImage = (ImageView) itemView.findViewById(R.id.iv_mkpay_payimage);
            }
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (onPaymentListClickListener != null) {
                onPaymentListClickListener.onPaymentListClicked(view, getPosition());
            }
        }
    }


    public interface OnPaymentListClickListener {
        public void onPaymentListClicked(View view, int position);
    }
}
