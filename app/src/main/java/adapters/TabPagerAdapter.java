package adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.ViewGroup;

import com.visualgov.ajwater.AccountsHomeActivity;

import fragments.AccountOverviewFragment;
import fragments.AccountOverviewFragment_;
import model.AccountsList;


/**
 * Created by Saraschandraa on 17-02-2015.
 */
public class TabPagerAdapter extends FragmentPagerAdapter {

    Context context;
    String[] tabs;
    int pagersize;
    boolean isTab;
    AccountsList accountsList;
    String tabHeader;

    public TabPagerAdapter(Context context, FragmentManager fm, AccountsList accountsList, boolean isTab) {
        super(fm);
        this.context = context;
        this.isTab = isTab;
        this.accountsList = accountsList;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new AccountOverviewFragment_();
        Bundle args = new Bundle();
        args.putSerializable("Account", accountsList.getAccountsList().get(position));
        args.putSerializable("AccountDetail", AccountsHomeActivity.getInstance().accountDetails.get(position));
        args.putInt("Position", position);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        if (!isTab) {
            if (accountsList.getAccountsList().size() > 3) {
                pagersize = 3;
            } else {
                pagersize = accountsList.getAccountsList().size();
            }
        } else {
            if (accountsList.getAccountsList().size() > 6) {
                pagersize = 7;
            } else {
                pagersize = accountsList.getAccountsList().size();
            }
        }
        return pagersize;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (!isTab) {
            if (accountsList.getAccountsList().size() > 3) {
                if (position <= 2) {
                    tabHeader = accountsList.getAccountsList().get(position).getAccountNumber();
                }
            } else {
                tabHeader = accountsList.getAccountsList().get(position).getAccountNumber();
            }
        } else {
            if (accountsList.getAccountsList().size() > 6) {
                if (position <= 5) {
                    tabHeader = accountsList.getAccountsList().get(position).getAccountNumber();
                }
            } else {
                tabHeader = accountsList.getAccountsList().get(position).getAccountNumber();
            }
        }

        return tabHeader;
    }


}
