package adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.visualgov.ajwater.R;

import java.util.ArrayList;

import model.AR;


/**
 * Created by Saraschandraa on 28-03-2015.
 */
public class BillingDetailsAdapter extends RecyclerView.Adapter<BillingDetailsAdapter.BillingDetailsHolder> {


    Context context;
    ArrayList<AR> arlist;

    public BillingDetailsAdapter(Context context, ArrayList<AR> arlist) {
        this.context = context;
        this.arlist = arlist;
    }


    @Override
    public BillingDetailsHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View billingDetailsView = LayoutInflater.from(context).inflate(R.layout.cell_billingdetailsinfolist, viewGroup, false);
        BillingDetailsHolder holder = new BillingDetailsHolder(billingDetailsView);
        return holder;
    }

    @Override
    public void onBindViewHolder(BillingDetailsHolder viewHolder, int position) {
        AR data = arlist.get(position);
        viewHolder.mTvTitle.setText(data.getAR());
        viewHolder.mTvDetail.setText("$ " + data.getValue());
    }

    @Override
    public int getItemCount() {
        return arlist.size();
    }

    class BillingDetailsHolder extends RecyclerView.ViewHolder {

        TextView mTvTitle, mTvDetail;

        public BillingDetailsHolder(View itemView) {
            super(itemView);
            mTvTitle = (TextView) itemView.findViewById(R.id.tv_dg_title);
            mTvDetail = (TextView) itemView.findViewById(R.id.tv_dg_detail);
        }
    }
}
