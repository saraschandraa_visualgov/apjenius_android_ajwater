package com.visualgov.ajwater;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FocusChange;
import org.androidannotations.annotations.TextChange;
import org.androidannotations.annotations.ViewById;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import info.hoang8f.android.segmented.SegmentedGroup;
import listener.WSListener;
import model.AccountDetails;
import model.Accounts;
import model.AccountsList;
import model.UserInfoList;
import util.Config;
import util.WSLoadingDialog;
import util.WebServiceCall;
import util.WebServiceUtil;

@EActivity(R.layout.activity_splash)
public class SplashActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {


    boolean isTablet;
    Animation animation;
    Handler splashHandler;
    WebServiceCall webServiceCall;
    String sUserName, sPassword;
    AccountsList accountsList;
    Accounts accounts;
    ArrayList<AccountDetails> accountDetailList;
    WSLoadingDialog wsLoadingDialog;

    @ViewById(R.id.sg_loginselector)
    SegmentedGroup loginSelector;

    @ViewById(R.id.rb_signin)
    RadioButton signinSelector;

    @ViewById(R.id.rb_register)
    RadioButton registerSelector;

    @ViewById(R.id.iv_splashtitle)
    ImageView ivLoginSplashImage;

    @ViewById(R.id.iv_splash)
    ImageView ivSplashImage;

    @ViewById(R.id.rl_splash)
    RelativeLayout splashLayout;

    @ViewById(R.id.rl_login)
    RelativeLayout loginLayout;

    @ViewById(R.id.et_sp_username)
    EditText etUsername;

    @ViewById(R.id.et_sp_pwd)
    EditText etPassword;

    @ViewById(R.id.iv_sp_username)
    ImageView ivUsername;

    @ViewById(R.id.iv_sp_pwd)
    ImageView ivPassword;


    @AfterViews
    void onViewLoaded() {
        wsLoadingDialog = new WSLoadingDialog(SplashActivity.this);
        loginLayout.setVisibility(View.GONE);
        splashLayout.setVisibility(View.VISIBLE);
        loginSelector.setOnCheckedChangeListener(this);
        signinSelector.setChecked(true);
        splashHandler = new Handler();
        splashHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                animateSplash();
            }
        }, 1000);
    }

    @TextChange(R.id.et_sp_username)
    void onUserNameTextChanged() {
        ivUsername.setVisibility(View.GONE);
    }

    @TextChange(R.id.et_sp_pwd)
    void onPassworTextChanged() {
        ivPassword.setVisibility(View.GONE);
    }

    @FocusChange(R.id.et_sp_username)
    void onUserNameFocusChanged(View view, boolean isFocusChanged) {
        if (isFocusChanged && etUsername.getText().length() >= 1) {
            ivUsername.setVisibility(View.GONE);
        } else {
            ivUsername.setVisibility(View.VISIBLE);
        }
    }

    @FocusChange(R.id.et_sp_pwd)
    void onPasswordFocusChanged(View view, boolean isFocusChanged) {
        if (isFocusChanged && etPassword.getText().length() >= 1) {
            ivPassword.setVisibility(View.GONE);
        } else {
            ivPassword.setVisibility(View.VISIBLE);
        }
    }

    @Click(R.id.btn_sp_signin)
    void onSigninClicked() {

        if (validateLoginCredentials()) {
            JSONObject params = null;
            try {
                params = new JSONObject();
                params.put("VGToken", Config.VGToken);
                params.put("UserName", sUserName);
                params.put("Password", sPassword);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            webServiceCall.doPostMethod(Config.URL_LOGIN, params, WebServiceUtil.WSTYPE.DO_POST, new WSListener() {
                @Override
                public void onRequestSent() {
                    wsLoadingDialog.showWSLoadingDialog();
                }

                @Override
                public void onRequestReceived(String response, Exception exception) {
                    if (response != null) {
                        try {
                            JSONObject result = new JSONObject(response);
                            JSONObject object = result.getJSONObject("Util");
                            JSONObject userInfo = object.getJSONObject("UserInfoList");
                            UserInfoList userInfoData = new UserInfoList(userInfo);
                            if (userInfoData.isLoginSuccess()) {
                                getAccountsInfo();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    private void getAccountsInfo() {
        JSONObject params = null;
        try {
            params = new JSONObject();
            params.put("VGToken", Config.VGToken);
            params.put("UserName", sUserName);
            params.put("Password", sPassword);
            params.put("Ticket", "SDSFSDFSD");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        webServiceCall.doPostMethod(Config.URL_GETACCOUNT, params, WebServiceUtil.WSTYPE.DO_POST, new WSListener() {
            @Override
            public void onRequestSent() {

            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                if (response != null) {
                    try {
                        JSONObject result = new JSONObject(response);
                        JSONObject util = result.getJSONObject("Util");
                        accountsList = new AccountsList(util);
                        if (accountsList.getAccountsList() != null && accountsList.getAccountsList().size() > 0) {
                            accountDetailList = new ArrayList<AccountDetails>();
                            for (int i = 0; i < accountsList.getAccountsList().size(); i++) {
                                accounts = accountsList.getAccountsList().get(i);
                                getAccountDetailInfo(accounts.getAccountNumber(), i);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void getAccountDetailInfo(String accountnumber, final int count) {
        JSONObject params = null;
        try {
            params = new JSONObject();
            params.put("VGToken", Config.VGToken);
            params.put("UserName", sUserName);
            params.put("Password", sPassword);
            params.put("Ticket", "SDSFSDFSD");
            params.put("UBAccount", accountnumber);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        webServiceCall.doPostMethod(Config.URL_GETACCOUNTDETAILS, params, WebServiceUtil.WSTYPE.DO_POST, new WSListener() {
            @Override
            public void onRequestSent() {

            }

            @Override
            public void onRequestReceived(String response, Exception exception) {
                if (response != null) {
                    try {
                        JSONObject result = new JSONObject(response);
                        JSONObject util = result.getJSONObject("Util");
                        AccountDetails accountDetails = new AccountDetails(util);
                        accountDetailList.add(accountDetails);
                        if (count == accountsList.getAccountsList().size() - 1) {
                            changeToAccountsHome();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void animateSplash() {
        animation = AnimationUtils.loadAnimation(SplashActivity.this, R.anim.image_anim);
        animation.reset();
        animation.setFillAfter(true);
        ivSplashImage.clearAnimation();
        ivSplashImage.startAnimation(animation);

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                splashLayout.setVisibility(View.GONE);
                loginLayout.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    private boolean validateLoginCredentials() {
        boolean isValid = false;
        sUserName = etUsername.getText().toString();
        sPassword = etPassword.getText().toString();
        if (!sUserName.equals("") && !sPassword.equals("")) {
            isValid = true;
        } else {
            isValid = false;
        }
        return isValid;
    }

    private void changeToAccountsHome() {
        wsLoadingDialog.hideWSLoadingDialog();
        Intent splashIntent = new Intent(SplashActivity.this, AccountsHomeActivity_.class);
        splashIntent.putExtra("AccountDetail", accountDetailList);
        splashIntent.putExtra("Accounts", accounts);
        splashIntent.putExtra("AccountList", accountsList);
        startActivity(splashIntent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isTablet = Config.isTablet(SplashActivity.this);
        Config.isTab = isTablet;
        if (isTablet) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        webServiceCall = new WebServiceCall(SplashActivity.this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_splash, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int viewId) {
        switch (viewId) {
            case R.id.rb_signin:
                signinSelector.setTextColor(getResources().getColor(R.color.color_white));
                registerSelector.setTextColor(getResources().getColor(R.color.color_blue));
                break;

            case R.id.rb_register:
                registerSelector.setTextColor(getResources().getColor(R.color.color_white));
                signinSelector.setTextColor(getResources().getColor(R.color.color_blue));
                break;
        }
    }
}
