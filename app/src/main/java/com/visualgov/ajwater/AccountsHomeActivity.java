package com.visualgov.ajwater;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.widget.TextView;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;

import fragments.AccountInfoPagerFragment;
import fragments.AccountInfoPagerFragment_;
import fragments.AutoPayDisclaimerFragment;
import fragments.AutoPayDisclaimerFragment_;
import fragments.AutoPayFragment1;
import fragments.AutoPayFragment1_;
import fragments.AutoPayFragment2;
import fragments.AutoPayFragment2_;
import fragments.AutoPayFragment3;
import fragments.AutoPayFragment3_;
import fragments.AutoPayFragment4;
import fragments.AutoPayFragment4_;
import fragments.BillingHistoryFragment;
import fragments.BillingHistoryFragment_;
import fragments.ChangepasswordFragment;
import fragments.ChangepasswordFragment_;
import fragments.ChoosePaymentFragment;
import fragments.ChoosePaymentFragment_;
import fragments.ConsumptionHistoryFragment;
import fragments.ConsumptionHistoryFragment_;
import fragments.MakePaymentFragment;
import fragments.MakePaymentFragment_;
import fragments.MakePaymentsFragments1;
import fragments.MakePaymentsFragments1_;
import fragments.PagerFragment;
import fragments.PagerFragment_;
import fragments.RequestAdditionalAccountFragment;
import fragments.RequestAdditionalAccountFragment_;
import model.AccountDetails;
import model.Accounts;
import model.AccountsList;

/**
 * Created by SaraschandraaM on 14/01/16.
 */

@EActivity(R.layout.activity_accountshome)
public class AccountsHomeActivity extends AppCompatActivity {

    @ViewById(R.id.drawerlayout)
    DrawerLayout drawerLayout;

    @ViewById(R.id.toolbar)
    Toolbar toolbar;

    @ViewById(R.id.tv_menu_ubaccount)
    TextView tvMenuUbAccount;

    @ViewById(R.id.tv_menu_mkpayment)
    TextView tvMenuMkPayment;

    @ViewById(R.id.tv_menu_autopay)
    TextView tvMenuAutoPay;

    @ViewById(R.id.tv_menu_ebilling)
    TextView tvMenuEbilling;

    @ViewById(R.id.tv_menu_newacc)
    TextView tvMenuNewAccount;

    @ViewById(R.id.tv_menu_changepwd)
    TextView tvMenuChangePwd;

    @ViewById(R.id.tv_menu_about)
    TextView tvMenuAbout;

    @ViewById(R.id.tv_menu_help)
    TextView tvMenuHelp;

    @ViewById(R.id.tv_toolbar_title)
    public TextView mTvToolbarTitle;


    public ArrayList<AccountDetails> accountDetails, selectedAccountDetails;
    public AccountsList accountsList;
    public Accounts accounts;
    public ArrayList<Accounts> selectedAccounts;

    FragmentTransaction fragmentTransaction, baseFragmentTransaction;

    ActionBarDrawerToggle mDrawerToggle;

    public static AccountsHomeActivity rootInstance;

    public static AccountsHomeActivity getInstance() {
        return rootInstance;
    }

    public static enum Fragments {
        PAGER_FRAGMENT, PAGER_ACCOUNTINFO, INFO_CONSHIS, INFO_BILLHIS, CHOOSE_PAYMENT, MAKE_PAYMENT, AUTOPAY_DISCLAIMER,
        AUT0PAY_1, AUTOPAY_2, AUTOPAY_3, AUTOPAY_4, CHANGE_PWD, REQ_ACC
    }

    public ArrayList<Fragments> fragmentsList;
    public Fragments viewingFragment;

    PagerFragment pagerFragment;
    AccountInfoPagerFragment accountInfoPagerFragment;
    ConsumptionHistoryFragment consumptionHistoryFragment;
    BillingHistoryFragment billingHistoryFragment;
    ChoosePaymentFragment choosePaymentFragment;
    MakePaymentFragment makePaymentFragment;
    AutoPayDisclaimerFragment autoPayDisclaimerFragment;
    AutoPayFragment1 autoPayFragment1;
    AutoPayFragment2 autoPayFragment2;
    AutoPayFragment3 autoPayFragment3;
    AutoPayFragment4 autoPayFragment4;
    ChangepasswordFragment changepasswordFragment;
    RequestAdditionalAccountFragment requestAdditionalAccountFragment;


    @AfterViews
    void onViewLoaded() {
        Intent data = getIntent();
        accountDetails = (ArrayList<AccountDetails>) data.getSerializableExtra("AccountDetail");
        accountsList = (AccountsList) data.getSerializableExtra("AccountList");
        accounts = (Accounts) data.getSerializableExtra("Accounts");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        selectedAccountDetails = new ArrayList<>();
        selectedAccounts = new ArrayList<>();
        fragmentsList = new ArrayList<>();
        mDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.open, R.string.close);
        drawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
        changeFragments(Fragments.PAGER_FRAGMENT, null);
    }

    @Click(R.id.rlmenu_accountlayout)
    void onUbAccountClicked() {
        drawerLayout.closeDrawer(Gravity.LEFT);
        setNavMenuTextColor(0);
        if (viewingFragment != Fragments.PAGER_FRAGMENT) {
            homeButtonPressed();
        }
    }

    @Click(R.id.rlmenu_mkpaymentlayout)
    void onMkPaymentClicked() {
        drawerLayout.closeDrawer(Gravity.LEFT);
        setNavMenuTextColor(1);
        Bundle args = new Bundle();
        args.putSerializable("AccountDetail", accountDetails);
        args.putSerializable("AccountList", accountsList);
        changeFragments(Fragments.CHOOSE_PAYMENT, args);
    }

    @Click(R.id.rlmenu_autppaylayout)
    void onAutoPayClicked() {
        drawerLayout.closeDrawer(Gravity.LEFT);
        setNavMenuTextColor(2);
        Bundle args = new Bundle();
        args.putSerializable("AccountDetail", accountDetails);
        args.putSerializable("AccountList", accountsList);
        changeFragments(Fragments.AUTOPAY_DISCLAIMER, args);
    }

    @Click(R.id.rlmenu_ebillinglayout)
    void onEbillingClicked() {
        drawerLayout.closeDrawer(Gravity.LEFT);
        setNavMenuTextColor(3);
    }

    @Click(R.id.rlmenu_additionalacc)
    void onReqAccountClicked() {
        drawerLayout.closeDrawer(Gravity.LEFT);
        setNavMenuTextColor(4);
        changeFragments(Fragments.REQ_ACC, null);
    }

    @Click(R.id.rlmenu_changepwdlayout)
    void onChangePasswordClicked() {
        drawerLayout.closeDrawer(Gravity.LEFT);
        changeFragments(Fragments.CHANGE_PWD, null);
    }

    @Click(R.id.iv_logoutbtn)
    void onLogoutClicked() {
        startActivity(new Intent(AccountsHomeActivity.this, SplashActivity_.class));
        finish();
    }

    public void changeFragments(Fragments fragments, Bundle args) {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();

        if (fragmentsList != null) {
            fragmentsList.add(fragments);
        }
        if (fragmentsList.size() != 0) {
            viewingFragment = fragmentsList.get(fragmentsList.size() - 1);
        }
        switch (fragments) {
            case PAGER_FRAGMENT:
                pagerFragment = new PagerFragment_();
                try {
                    pagerFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.replace(R.id.fl_fragmentholder, pagerFragment);
                fragmentTransaction.commit();
                break;

            case PAGER_ACCOUNTINFO:
                accountInfoPagerFragment = new AccountInfoPagerFragment_();
                try {
                    accountInfoPagerFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.top_in, R.anim.bottom_out, R.anim.bottom_in, R.anim.top_out);
                fragmentTransaction.replace(R.id.fl_fragmentholder, accountInfoPagerFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case INFO_CONSHIS:
                consumptionHistoryFragment = new ConsumptionHistoryFragment_();
                try {
                    consumptionHistoryFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.top_in, R.anim.bottom_out, R.anim.bottom_in, R.anim.top_out);
                fragmentTransaction.add(R.id.fl_fragmentholder, consumptionHistoryFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case INFO_BILLHIS:
                billingHistoryFragment = new BillingHistoryFragment_();
                try {
                    billingHistoryFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.top_in, R.anim.bottom_out, R.anim.bottom_in, R.anim.top_out);
                fragmentTransaction.add(R.id.fl_fragmentholder, billingHistoryFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case CHOOSE_PAYMENT:
                choosePaymentFragment = new ChoosePaymentFragment_();
                try {
                    choosePaymentFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.top_in, R.anim.bottom_out, R.anim.bottom_in, R.anim.top_out);
                fragmentTransaction.add(R.id.fl_fragmentholder, choosePaymentFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case MAKE_PAYMENT:
                makePaymentFragment = new MakePaymentFragment_();
                try {
                    makePaymentFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
                fragmentTransaction.replace(R.id.fl_fragmentholder, makePaymentFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case AUTOPAY_DISCLAIMER:
                autoPayDisclaimerFragment = new AutoPayDisclaimerFragment_();
                try {
                    autoPayDisclaimerFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.top_in, R.anim.bottom_out, R.anim.bottom_in, R.anim.top_out);
                fragmentTransaction.add(R.id.fl_fragmentholder, autoPayDisclaimerFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case AUT0PAY_1:
                autoPayFragment1 = new AutoPayFragment1_();
                try {
                    autoPayFragment1.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
                fragmentTransaction.replace(R.id.fl_fragmentholder, autoPayFragment1);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case AUTOPAY_2:
                autoPayFragment2 = new AutoPayFragment2_();
                try {
                    autoPayFragment2.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
                fragmentTransaction.replace(R.id.fl_fragmentholder, autoPayFragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case AUTOPAY_3:
                autoPayFragment3 = new AutoPayFragment3_();
                try {
                    autoPayFragment3.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
                fragmentTransaction.replace(R.id.fl_fragmentholder, autoPayFragment3);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case AUTOPAY_4:
                autoPayFragment4 = new AutoPayFragment4_();
                try {
                    autoPayFragment1.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.left_in, R.anim.right_out, R.anim.right_in, R.anim.left_out);
                fragmentTransaction.replace(R.id.fl_fragmentholder, autoPayFragment4);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case CHANGE_PWD:
                changepasswordFragment = new ChangepasswordFragment_();
                try {
                    changepasswordFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.top_in, R.anim.bottom_out, R.anim.bottom_in, R.anim.top_out);
                fragmentTransaction.add(R.id.fl_fragmentholder, changepasswordFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;

            case REQ_ACC:
                requestAdditionalAccountFragment = new RequestAdditionalAccountFragment_();
                try {
                    requestAdditionalAccountFragment.setArguments(args);
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
                fragmentTransaction.setCustomAnimations(R.anim.top_in, R.anim.bottom_out, R.anim.bottom_in, R.anim.top_out);
                fragmentTransaction.add(R.id.fl_fragmentholder, requestAdditionalAccountFragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                break;
        }
    }

    public void setNavMenuTextColor(int menu) {
        switch (menu) {
            case 0:
                tvMenuUbAccount.setTextColor(getResources().getColor(R.color.color_menu_ubaccount));
                tvMenuMkPayment.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuAutoPay.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuEbilling.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuNewAccount.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                break;

            case 1:
                tvMenuUbAccount.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuMkPayment.setTextColor(getResources().getColor(R.color.color_menu_makepayment));
                tvMenuAutoPay.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuEbilling.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuNewAccount.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                break;

            case 2:
                tvMenuUbAccount.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuMkPayment.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuAutoPay.setTextColor(getResources().getColor(R.color.color_menu_autopay));
                tvMenuEbilling.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuNewAccount.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                break;

            case 3:
                tvMenuUbAccount.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuMkPayment.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuAutoPay.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuEbilling.setTextColor(getResources().getColor(R.color.color_menu_ebilling));
                tvMenuNewAccount.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                break;

            case 4:
                tvMenuUbAccount.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuMkPayment.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuAutoPay.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuEbilling.setTextColor(getResources().getColor(R.color.color_menu_unselected));
                tvMenuNewAccount.setTextColor(getResources().getColor(R.color.color_menu_reqacc));
                break;

        }
    }

    public void homeButtonPressed() {
        FragmentManager fm = getSupportFragmentManager();
        int backStackCount = getSupportFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < backStackCount; i++) {
            int backStackId = getSupportFragmentManager().getBackStackEntryAt(i).getId();
            fm.popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        changeFragments(Fragments.PAGER_FRAGMENT, null);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootInstance = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (fragmentsList.size() > 1) {
            if (fragmentsList.size() != 0) {
                viewingFragment = fragmentsList.get(fragmentsList.size() - 2);
            }
            fragmentsList.remove(fragmentsList.size() - 1);
        }
    }
}
