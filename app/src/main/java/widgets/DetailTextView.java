package widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.TextView;

import com.visualgov.ajwater.R;

/**
 * Created by SaraschandraaM on 20/01/16.
 */
public class DetailTextView extends TextView {

    public DetailTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            TypedArray attributes = context.obtainStyledAttributes(attrs, R.styleable.FontCustom, R.attr.typeface, 0);

            String typefaceDesc = attributes.getString(R.styleable.FontCustom_typeface);

            // If not set in the style, attempt to pull from the messageTypeface
//            if (typefaceDesc == null) {
//                typefaceDesc = attributes.getString(R.styleable.FontCustom_messageTypeface);
//            }
//
//            if (typefaceDesc != null) {
            Typeface typeface = Typeface.createFromAsset(context.getAssets(), "Roboto-Medium.ttf");
            ;
            this.setTypeface(typeface);
            this.setTextColor(context.getResources().getColor(R.color.color_black));
            this.setTextSize(TypedValue.COMPLEX_UNIT_SP, context.getResources().getInteger(R.integer.detailtext));
//            } else {
//            }
        }
    }
}
