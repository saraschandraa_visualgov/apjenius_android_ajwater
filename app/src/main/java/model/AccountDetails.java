package model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kandan on 1/12/2016.
 */
public class AccountDetails implements Serializable {

    ArrayList<AR> arList;
    ArrayList<BillingHistory> billingHistories;
    ArrayList<WaterCons> waterConses;
    UBDetailsList ubDetailsList;

    public ArrayList<AR> getArList() {
        return arList;
    }

    public void setArList(ArrayList<AR> arList) {
        this.arList = arList;
    }

    public ArrayList<BillingHistory> getBillingHistories() {
        return billingHistories;
    }

    public void setBillingHistories(ArrayList<BillingHistory> billingHistories) {
        this.billingHistories = billingHistories;
    }

    public ArrayList<WaterCons> getWaterConses() {
        return waterConses;
    }

    public void setWaterConses(ArrayList<WaterCons> waterConses) {
        this.waterConses = waterConses;
    }

    public UBDetailsList getUbDetailsList() {
        return ubDetailsList;
    }

    public void setUbDetailsList(UBDetailsList ubDetailsList) {
        this.ubDetailsList = ubDetailsList;
    }

    public AccountDetails() {

    }

    public AccountDetails(JSONObject jsonObject) {
        try {
            arList = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("AR");
            if (json == null) {
                JSONArray jsonArray = jsonObject.getJSONArray("AR");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jObj = (JSONObject) jsonArray.get(i);
                    AR data = new AR(jObj);
                    arList.add(data);
                }
            } else {
                AR data = new AR(json);
                arList.add(data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONObject json = jsonObject.getJSONObject("UBDetailsList");
            ubDetailsList = new UBDetailsList(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            waterConses = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("WaterCons");
            if (json == null) {
                JSONArray jsonArray = jsonObject.getJSONArray("WaterCons");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jObj = (JSONObject) jsonArray.get(i);
                    WaterCons data = new WaterCons(jObj);
                    waterConses.add(data);
                }
            } else {
                WaterCons data = new WaterCons(json);
                waterConses.add(data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            billingHistories = new ArrayList<>();
            JSONObject json = jsonObject.optJSONObject("BillingHistory");
            if (json == null) {
                JSONArray jsonArray = jsonObject.getJSONArray("BillingHistory");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jObj = (JSONObject) jsonArray.get(i);
                    BillingHistory data = new BillingHistory(jObj);
                    billingHistories.add(data);
                }
            } else {
                BillingHistory data = new BillingHistory(json);
                billingHistories.add(data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
