package model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by kandan on 1/12/2016.
 */
public class Accounts implements Serializable {

    String AccountAddress;
    String AccountNumber;

    public String getAccountAddress() {
        return AccountAddress;
    }

    public void setAccountAddress(String accountAddress) {
        AccountAddress = accountAddress;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public Accounts(){

    }

    public Accounts(JSONObject jsonObject){
        try {
            AccountAddress = jsonObject.getString("AccountAddress");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try {
            AccountNumber = jsonObject.getString("AccountNumber");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
