package model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by kandan on 1/12/2016.
 */
public class AR implements Serializable {

    String AR;
    String Value;

    public String getAR() {
        return AR;
    }

    public void setAR(String AR) {
        this.AR = AR;
    }

    public String getValue() {
        return Value;
    }

    public void setValue(String value) {
        Value = value;
    }

    public AR(){

    }

    public AR(JSONObject jsonObject){
        try{
           AR = jsonObject.getString("AR");
        }catch (JSONException e){
            e.printStackTrace();
        }

        try{
            Value = jsonObject.getString("Value");
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
