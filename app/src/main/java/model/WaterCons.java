package model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by kandan on 1/12/2016.
 */
public class WaterCons implements Serializable {

    String Amount;
    String Cons;
    String Date;
    String MeterCode;
    String MeterNumber;
    String Reading;
    String Type;

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getCons() {
        return Cons;
    }

    public void setCons(String cons) {
        Cons = cons;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getMeterCode() {
        return MeterCode;
    }

    public void setMeterCode(String meterCode) {
        MeterCode = meterCode;
    }

    public String getMeterNumber() {
        return MeterNumber;
    }

    public void setMeterNumber(String meterNumber) {
        MeterNumber = meterNumber;
    }

    public String getReading() {
        return Reading;
    }

    public void setReading(String reading) {
        Reading = reading;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public WaterCons (JSONObject jsonObject){
        try {
            Amount = jsonObject.getString("Amount");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Cons = jsonObject.getString("Cons");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Date = jsonObject.getString("Date");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            MeterCode=jsonObject.getString("MeterCode");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            MeterNumber=jsonObject.getString("MeterNumber");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Reading=jsonObject.getString("Reading");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Type=jsonObject.getString("Type");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }
}
