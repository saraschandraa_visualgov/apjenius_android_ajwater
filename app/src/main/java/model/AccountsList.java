package model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by kandan on 1/12/2016.
 */
public class AccountsList implements Serializable {

    ArrayList<Accounts> accountsList;

    public ArrayList<Accounts> getAccountsList() {
        return accountsList;
    }

    public void setAccountsList(ArrayList<Accounts> accountsList) {
        this.accountsList = accountsList;
    }

    public AccountsList() {

    }

    public AccountsList(JSONObject jsonObject) {
        accountsList = new ArrayList<>();
        try {
            JSONObject json = jsonObject.optJSONObject("Accounts");
            if (json == null) {
                JSONArray jsonArray = jsonObject.getJSONArray("Accounts");
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jObj = (JSONObject) jsonArray.get(i);
                    Accounts data = new Accounts(jObj);
                    accountsList.add(data);
                }
            } else {
                Accounts data = new Accounts(json);
                accountsList.add(data);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
