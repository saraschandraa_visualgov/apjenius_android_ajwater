package model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by kandan on 1/12/2016.
 */
public class UBDetailsList implements Serializable {

    String Address1;
    String Address2;
    String AmountDue;
    String AR;
    String CurrentAmount;
    String HasAR;
    String HomeNumber;
    String IsDelinquent;
    String IsResultValid;
    String MoveInDate;
    String Name;
    String NumberOfMeters;
    String Over120Days;
    String Over30Days;
    String Over60Days;
    String Over90Days;
    String PendingPayments;
    String PreviousBalance;
    String PTDAdjustment;
    String PTDPayments;
    String PTDPenalties;
    String ServiceAddress;
    String workNumber;


    public String getAddress1() {
        return Address1;
    }

    public void setAddress1(String address1) {
        Address1 = address1;
    }

    public String getAddress2() {
        return Address2;
    }

    public void setAddress2(String address2) {
        Address2 = address2;
    }

    public String getAmountDue() {
        return AmountDue;
    }

    public void setAmountDue(String amountDue) {
        AmountDue = amountDue;
    }

    public String getAR() {
        return AR;
    }

    public void setAR(String AR) {
        this.AR = AR;
    }

    public String getCurrentAmount() {
        return CurrentAmount;
    }

    public void setCurrentAmount(String currentAmount) {
        CurrentAmount = currentAmount;
    }

    public String getHomeNumber() {
        return HomeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        HomeNumber = homeNumber;
    }

    public String getHasAR() {
        return HasAR;
    }

    public void setHasAR(String hasAR) {
        HasAR = hasAR;
    }

    public String getIsDelinquent() {
        return IsDelinquent;
    }

    public void setIsDelinquent(String isDelinquent) {
        IsDelinquent = isDelinquent;
    }

    public String getIsResultValid() {
        return IsResultValid;
    }

    public void setIsResultValid(String isResultValid) {
        IsResultValid = isResultValid;
    }

    public String getMoveInDate() {
        return MoveInDate;
    }

    public void setMoveInDate(String moveInDate) {
        MoveInDate = moveInDate;
    }

    public String getNumberOfMeters() {
        return NumberOfMeters;
    }

    public void setNumberOfMeters(String numberOfMeters) {
        NumberOfMeters = numberOfMeters;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getOver120Days() {
        return Over120Days;
    }

    public void setOver120Days(String over120Days) {
        Over120Days = over120Days;
    }

    public String getOver60Days() {
        return Over60Days;
    }

    public void setOver60Days(String over60Days) {
        Over60Days = over60Days;
    }

    public String getOver30Days() {
        return Over30Days;
    }

    public void setOver30Days(String over30Days) {
        Over30Days = over30Days;
    }

    public String getOver90Days() {
        return Over90Days;
    }

    public void setOver90Days(String over90Days) {
        Over90Days = over90Days;
    }

    public String getPendingPayments() {
        return PendingPayments;
    }

    public void setPendingPayments(String pendingPayments) {
        PendingPayments = pendingPayments;
    }

    public String getPreviousBalance() {
        return PreviousBalance;
    }

    public void setPreviousBalance(String previousBalance) {
        PreviousBalance = previousBalance;
    }

    public String getPTDAdjustment() {
        return PTDAdjustment;
    }

    public void setPTDAdjustment(String PTDAdjustment) {
        this.PTDAdjustment = PTDAdjustment;
    }

    public String getPTDPayments() {
        return PTDPayments;
    }

    public void setPTDPayments(String PTDPayments) {
        this.PTDPayments = PTDPayments;
    }

    public String getPTDPenalties() {
        return PTDPenalties;
    }

    public void setPTDPenalties(String PTDPenalties) {
        this.PTDPenalties = PTDPenalties;
    }

    public String getServiceAddress() {
        return ServiceAddress;
    }

    public void setServiceAddress(String serviceAddress) {
        ServiceAddress = serviceAddress;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public UBDetailsList (JSONObject jsonObject){
        try {
            Address1 = jsonObject.getString("Address1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Address2 = jsonObject.getString("Address2");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            AmountDue = jsonObject.getString("AmountDue");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            AR = jsonObject.getString("AR");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            CurrentAmount = jsonObject.getString("CurrentAmount");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            HasAR = jsonObject.getString("HasAR");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            HomeNumber = jsonObject.getString("HomeNumber");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            IsDelinquent = jsonObject.getString("IsDelinquent");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            IsResultValid = jsonObject.getString("IsResultValid");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            MoveInDate = jsonObject.getString("MoveInDate");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Name = jsonObject.getString("Name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            NumberOfMeters = jsonObject.getString("NumberOfMeters");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Over120Days = jsonObject.getString("Over120Days");

        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Over30Days = jsonObject.getString("Over30Days");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Over60Days = jsonObject.getString("Over60Days");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            Over90Days = jsonObject.getString("Over90Days");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            PendingPayments = jsonObject.getString("PendingPayments");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            PreviousBalance = jsonObject.getString("PreviousBalance");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            PTDAdjustment = jsonObject.getString("PTDAdjustment");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            PTDPayments =jsonObject.getString("PTDPayments");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            PTDPenalties =jsonObject.getString("PTDPenalties");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            ServiceAddress =jsonObject.getString("ServiceAddress");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            workNumber =jsonObject.getString("workNumber");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
