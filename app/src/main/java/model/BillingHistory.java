package model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by kandan on 1/12/2016.
 */
public class BillingHistory implements Serializable {

    String Amount;
    String BillNo;
    String PayType;
    String RunningBalance;
    String TransDate;
    String TransType;

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getBillNo() {
        return BillNo;
    }

    public void setBillNo(String billNo) {
        BillNo = billNo;
    }

    public String getPayType() {
        return PayType;
    }

    public void setPayType(String payType) {
        PayType = payType;
    }

    public String getRunningBalance() {
        return RunningBalance;
    }

    public void setRunningBalance(String runningBalance) {
        RunningBalance = runningBalance;
    }

    public String getTransDate() {
        return TransDate;
    }

    public void setTransDate(String transDate) {
        TransDate = transDate;
    }

    public String getTransType() {
        return TransType;
    }

    public void setTransType(String transType) {
        TransType = transType;
    }

    public BillingHistory(JSONObject jsonObject){
        try{
            Amount = jsonObject.getString("Amount");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            BillNo = jsonObject.getString("BillNo");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            PayType = jsonObject.getString("PayType");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try{
            RunningBalance = jsonObject.getString("RunningBalance");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            TransDate = jsonObject.getString("TransDate");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            TransType = jsonObject.getString("TransType");
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
