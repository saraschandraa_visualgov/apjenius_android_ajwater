package model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by kandan on 1/12/2016.
 */
public class UserInfoList implements Serializable {

    String Message;
    String Ticket;
    boolean isLoginSuccess;

    public String getTicket() {
        return Ticket;
    }

    public void setTicket(String ticket) {
        Ticket = ticket;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public boolean isLoginSuccess() {
        return isLoginSuccess;
    }

    public void setIsLoginSuccess(boolean isLoginSuccess) {
        this.isLoginSuccess = isLoginSuccess;
    }

    public UserInfoList() {

    }

    public UserInfoList(JSONObject jsonObject) {

        try {
            Message = jsonObject.getString("Message");
            if(Message.equals("Success")) {
                isLoginSuccess = true;
            }else {
                isLoginSuccess = false;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        try {
            Ticket = jsonObject.getString("Ticket");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
